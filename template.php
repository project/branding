<?php global $justify;
global $magmenu;
global $menuh;
global $ocmenu;
global $columncounter;
$columncounter = 0;
$magmenu = false; ?>
<?php $menuh = false; ?>
<?php global $vmagmenu;
global $smenuh;
global $vmenuh;
$vmagmenu = false; ?>
<?php $smenuh = true; ?>
<?php $vmenuh = false; ?>
<?php $justify = false; ?>
<?php
require_once('theme_methods.php');
drupal_add_css(base_path() . path_to_theme() . '/menuie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
drupal_add_css(base_path() . path_to_theme(). '/vmenuie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
function brand_drupal_js_alter(&$javascript) {
$jquery_path = path_to_theme() . '/jquery.js';
$javascript[$jquery_path] = $javascript['misc/jquery.js'];
$javascript[$jquery_path]['version'] = '1.10.2';
$javascript[$jquery_path]['data'] = $jquery_path;
unset($javascript['misc/jquery.js']);
unset($javascript['misc/ui/jquery.ui.core.min.js']);
unset($javascript['misc/jquery.ba-bbq.js']);
unset($javascript['modules/overlay/overlay-parent.js']);
unset($javascript['misc/jquery.cookie.js']);
 }
function brand_drupal_button($variables) {
$element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
 element_set_attributes($element, array('id', 'name', 'value'));
 $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
 if (!empty($element['#attributes']['disabled'])) {
 $element['#attributes']['class'][] = 'form-button-disabled';
  }
 return '<div> <span onmouseout="this.className=\'ttr_button\';" onmouseover="this.className=\'ttr_button_hover1\';" class="ttr_button"><input class="btn btn-default"'. drupal_attributes($element['#attributes'
]) .' /></span> <div style="clear:both;"></div> </div>';
}
function brand_drupal_breadcrumb($variables) {
$breadcrumb = $variables['breadcrumb'];
if (!empty($breadcrumb)) {
$output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
$crumbs = '<ol class="breadcrumb">';
$array_size = count($breadcrumb);
$i = 0;
while ( $i < $array_size) {
 $crumbs .= '<li>' . $breadcrumb[$i] . '</li>';
$i++;
}
$crumbs .= '<li><span class="active">'. drupal_get_title() .'</span></li></ol>';
return $crumbs;
}
}
function brand_drupal_feed_icon($variables) {
$theme_path = drupal_get_path('theme','brand_drupal');
$text = t('Subscribe to @feed-title', array('@feed-title' => $variables['title']));
return l('', $variables['url'], array('html' => TRUE, 'attributes' => array('class' => array('ttr_footer_rss'), 'title' => $text)));
}
function brand_drupal_pager($variables) {
$tags = $variables['tags'];
$element = $variables['element'];
$parameters = $variables['parameters'];
$quantity = $variables['quantity'];
global $pager_page_array, $pager_total;
$pager_middle = ceil($quantity / 2);
$pager_current = $pager_page_array[$element] + 1;
$pager_first = $pager_current - $pager_middle + 1;
$pager_last = $pager_current + $quantity - $pager_middle;
$pager_max = $pager_total[$element];
$i = $pager_first;
if ($pager_last > $pager_max) {
$i = $i + ($pager_max - $pager_last);
$pager_last = $pager_max;
}
if ($i <= 0) {
$pager_last = $pager_last + (1 - $i);
$i = 1;
}
$li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('first')), 'element' => $element, 'parameters' => $parameters));
$li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
$li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
$li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last')), 'element' => $element, 'parameters' => $parameters));
if ($pager_total[$element] > 1) {
if ($li_first) {
$items[] = array(
'class' => array('pager-first'),
'data' => $li_first,
);
}
if ($li_previous) {
$items[] = array(
'class' => array('pager-previous'),
'data' => $li_previous,
);
}
if ($i != $pager_max) {
if ($i > 1) {
$items[] = array(
'class' => array('pager-ellipsis'),
'data' => 'â¦',
);
}
for (; $i <= $pager_last && $i <= $pager_max; $i++) {
if ($i < $pager_current) {
$items[] = array(
'class' => array('pager-item'),
'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
);
}
if ($i == $pager_current) {
$items[] = array(
'class' => array('pager-current'),
'data' => '<span>'.$i.'</span>',
);
}
if ($i > $pager_current) {
$items[] = array(
'class' => array('pager-item'),
'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
);
}
}
if ($i < $pager_max) {
$items[] = array(
'class' => array('pager-ellipsis'),
'data' => 'â¦',
);
}
}
if ($li_next) {
$items[] = array(
'class' => array('pager-next'),
'data' => $li_next,
);
}
if ($li_last) {
$items[] = array(
'class' => array('pager-last'),
'data' => $li_last,
);
}
return '<h2 class="element-invisible">' . t('Pages') . '</h2><div class="pagination">' . theme('item_list', array(
'items' => $items,
'attributes' => array('class' => array('pager')),
)).'</div>';
}
}
