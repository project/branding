<?php $theme_path = base_path() . path_to_theme(); ?>
<div class="ttr_banner_header">
<?php
if( !empty($page['headerabovecolumn1'])|| !empty($page['headerabovecolumn2'])|| !empty($page['headerabovecolumn3'])||!empty($page['headerabovecolumn4'])):
?>
<div class="ttr_banner_header_inner_above0">
<?php
$showcolumn= !empty($page['headerabovecolumn1']);
?>
<?php if($showcolumn): ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerabovecolumn1">
<?php print render($page['headerabovecolumn1']); ?>
</div>
</div>
<?php else: ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['headerabovecolumn2']);
?>
<?php if($showcolumn): ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerabovecolumn2">
<?php print render($page['headerabovecolumn2']); ?>
</div>
</div>
<?php else: ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-sm-block visible-md-block visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['headerabovecolumn3']);
?>
<?php if($showcolumn): ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerabovecolumn3">
<?php print render($page['headerabovecolumn3']); ?>
</div>
</div>
<?php else: ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['headerabovecolumn4']);
?>
<?php if($showcolumn): ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerabovecolumn4">
<?php print render($page['headerabovecolumn4']); ?>
</div>
</div>
<?php else: ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
</div>
</div>
<div class="clearfix"></div>
<?php endif; ?>
</div>
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<header id="ttr_header">
<div id="ttr_header_inner">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="innermenu"><nav id="ttr_menu" class="navbar-default navbar">
<div id="ttr_menu_inner_in"> 
<div class="menuforeground">
</div>
<div id="navigationmenu">
<div class="navbar-header">
<button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
<span class="sr-only">
</span>
<span class="icon-bar">
</span>
<span class="icon-bar">
</span>
<span class="icon-bar">
</span>
</button>
<a href="http://www.templatetoaster.com">
<img class="ttr_menu_logo" src="<?php echo $theme_path?>/menulogo.png" alt="<?php print t('Home'); ?>" />
</a>
</div>
<div class="menu-center collapse navbar-collapse">
<ul class="ttr_menu_items nav navbar-nav navbar-right">
<?php if ($main_menu){ echo generate_menu("ttr_");}?>
<?php $theme_path = base_path() . path_to_theme(); ?>
</ul>
</div>
</div>
</div>
</nav>

</div>
<div class="ttr_headershape01">
<div class="html_content"><p style="text-align:Center;"><span style="font-size:1.14285714285714em;color:rgba(183,126,75,1);">Scroll Down</span></p><p><span class="ttr_image" style="float:none;display:block;text-align:center;overflow:hidden;margin:0px 0px 0px 0px;"><span><img src="<?php echo $theme_path; ?>/images/235.png" class="ttr_uniform" style="max-width:14px;max-height:9px;" /></span><br /></span><br style="font-size:1.07142857142857em;color:rgba(183,126,75,1);" /></p></div>
</div>
<?php  print $feed_icons;   ?>
<a href="http://www.facebook.com/TemplateToaster"class="ttr_header_facebook" target="_self">
</a>
<a href="http://www.templatetoaster.com"class="ttr_header_linkedin"target="_self">
</a>
<a href="http://twitter.com/templatetoaster"class="ttr_header_twitter" target="_self">
</a>
<a href="http://www.templatetoaster.com"class="ttr_header_googleplus" target="_self">
</a>
</div>
</header>
<div class="ttr_banner_header">
<?php
if( !empty($page['headerbelowcolumn1'])|| !empty($page['headerbelowcolumn2'])|| !empty($page['headerbelowcolumn3'])||!empty($page['headerbelowcolumn4'])):
?>
<div class="ttr_banner_header_inner_below0">
<?php
$showcolumn= !empty($page['headerbelowcolumn1']);
?>
<?php if($showcolumn): ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerbelowcolumn1">
<?php print render($page['headerbelowcolumn1']); ?>
</div>
</div>
<?php else: ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['headerbelowcolumn2']);
?>
<?php if($showcolumn): ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerbelowcolumn2">
<?php print render($page['headerbelowcolumn2']); ?>
</div>
</div>
<?php else: ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-sm-block visible-md-block visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['headerbelowcolumn3']);
?>
<?php if($showcolumn): ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerbelowcolumn3">
<?php print render($page['headerbelowcolumn3']); ?>
</div>
</div>
<?php else: ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['headerbelowcolumn4']);
?>
<?php if($showcolumn): ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="headerbelowcolumn4">
<?php print render($page['headerbelowcolumn4']); ?>
</div>
</div>
<?php else: ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
</div>
</div>
<div class="clearfix"></div>
<?php endif; ?>
</div>
<div id="ttr_page" class="container">
<div id="ttr_content_and_sidebar_container">
<div id="ttr_content">
<div id="ttr_content_margin" class="container-fluid">
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<?php if ($breadcrumb): ?>
<?php print $breadcrumb; ?>
<?php endif; ?>
<?php
if( !empty($page['contenttopcolumn1'])|| !empty($page['contenttopcolumn2'])|| !empty($page['contenttopcolumn3'])||!empty($page['contenttopcolumn4'])):
?>
<div class="contenttopcolumn0">
<?php
$showcolumn= !empty($page['contenttopcolumn1']);
?>
<?php if($showcolumn): ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="topcolumn1">
<?php print render($page['contenttopcolumn1']); ?>
</div>
</div>
<?php else: ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['contenttopcolumn2']);
?>
<?php if($showcolumn): ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="topcolumn2">
<?php print render($page['contenttopcolumn2']); ?>
</div>
</div>
<?php else: ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-sm-block visible-md-block visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['contenttopcolumn3']);
?>
<?php if($showcolumn): ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="topcolumn3">
<?php print render($page['contenttopcolumn3']); ?>
</div>
</div>
<?php else: ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['contenttopcolumn4']);
?>
<?php if($showcolumn): ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="topcolumn4">
<?php print render($page['contenttopcolumn4']); ?>
</div>
</div>
<?php else: ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
</div>
</div>
<div class="clearfix"></div>
<?php endif; ?>
<div style="clear:both;">
</div>
 <?php if ($tabs): ?>
<div style="clear:both;" class="tabs"><?php print render($tabs); ?></div>
 <?php endif; ?>
<?php print render($page['help']); ?>
 <?php print $messages; ?>
<?php if ($action_links): ?>
<ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
<div class="row">
<?php print render($page['content']); ?>
</div>
<?php
if( !empty($page['contentbottomcolumn1'])|| !empty($page['contentbottomcolumn2'])|| !empty($page['contentbottomcolumn3'])||!empty($page['contentbottomcolumn4'])):
?>
<div class="contentbottomcolumn0">
<?php
$showcolumn= !empty($page['contentbottomcolumn1']);
?>
<?php if($showcolumn): ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="bottomcolumn1">
<?php print render($page['contentbottomcolumn1']); ?>
</div>
</div>
<?php else: ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['contentbottomcolumn2']);
?>
<?php if($showcolumn): ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="bottomcolumn2">
<?php print render($page['contentbottomcolumn2']); ?>
</div>
</div>
<?php else: ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-sm-block visible-md-block visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['contentbottomcolumn3']);
?>
<?php if($showcolumn): ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="bottomcolumn3">
<?php print render($page['contentbottomcolumn3']); ?>
</div>
</div>
<?php else: ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['contentbottomcolumn4']);
?>
<?php if($showcolumn): ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="bottomcolumn4">
<?php print render($page['contentbottomcolumn4']); ?>
</div>
</div>
<?php else: ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
</div>
</div>
<div class="clearfix"></div>
<?php endif; ?>
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
</div> 
</div> 
<div style="clear:both;">
</div>
<div class="footer-widget-area">
<div class="footer-widget-area_left_border_image">
<div class="footer-widget-area_right_border_image">
<div class="footer-widget-area_inner">
<?php
if( !empty($page['footerabovecolumn1'])|| !empty($page['footerabovecolumn2'])|| !empty($page['footerabovecolumn3'])||!empty($page['footerabovecolumn4'])):
?>
<div class="ttr_footer-widget-area_inner_above0">
<?php
$showcolumn= !empty($page['footerabovecolumn1']);
?>
<?php if($showcolumn): ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerabovecolumn1">
<?php print render($page['footerabovecolumn1']); ?>
</div>
</div>
<?php else: ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['footerabovecolumn2']);
?>
<?php if($showcolumn): ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerabovecolumn2">
<?php print render($page['footerabovecolumn2']); ?>
</div>
</div>
<?php else: ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-sm-block visible-md-block visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['footerabovecolumn3']);
?>
<?php if($showcolumn): ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerabovecolumn3">
<?php print render($page['footerabovecolumn3']); ?>
</div>
</div>
<?php else: ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['footerabovecolumn4']);
?>
<?php if($showcolumn): ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerabovecolumn4">
<?php print render($page['footerabovecolumn4']); ?>
</div>
</div>
<?php else: ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
</div>
</div>
<div class="clearfix"></div>
<?php endif; ?>
</div>
</div>
</div>
</div>
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<footer id="ttr_footer">
<div id="ttr_footer_top_for_widgets">
<div  class="ttr_footer_top_for_widgets_inner">
<?php
if (   !empty($page['leftfooterarea']) || !empty($page['centerfooterarea']) || !empty($page['rightfooterarea'])):
?>
<div class="footer-widget-area_fixed">
<div style="margin:0 auto;">
<?php if(!empty($page['leftfooterarea'])): ?>
<div  id="first" class="widget-area  col-lg-4 col-md-4 col-sm-4 col-xs-12">
<?php print render($page['leftfooterarea']); ?>
</div>
<div class="clearfix visible-xs"></div>
<?php else: ?>
<div  id="first" class="widget-area  col-lg-4 col-md-4 col-sm-4 col-xs-12">
&nbsp;
</div>
<div class="clearfix visible-xs"></div>
<?php endif; ?>
<?php if(!empty($page['centerfooterarea'])): ?>
<div  id="second" class="widget-area  col-lg-4 col-md-4 col-sm-4 col-xs-12">
<?php print render($page['centerfooterarea']); ?>
</div>
<div class="clearfix visible-xs"></div>
<?php else: ?>
<div  id="second" class="widget-area  col-lg-4 col-md-4 col-sm-4 col-xs-12">
&nbsp;
</div>
<div class="clearfix visible-xs"></div>
<?php endif; ?>
<?php if(!empty($page['rightfooterarea'])): ?>
<div  id="third" class="widget-area  col-lg-4 col-md-4 col-sm-4 col-xs-12">
<?php print render($page['rightfooterarea']); ?>
</div>
<div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>
<?php else: ?>
<div  id="third" class="widget-area  col-lg-4 col-md-4 col-sm-4 col-xs-12">
&nbsp;
</div>
<div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>
<?php endif; ?>
</div>
</div>
<?php endif; ?>
</div>
</div>
<div class="ttr_footer_bottom_footer">
<div class="ttr_footer_bottom_footer_inner">
<div id="ttr_footer_designed_by_links">
<a href="http://templatetoaster.com" target="_self"> Drupal Theme </a>
<span id="ttr_footer_designed_by"> Designed With TemplateToaster </span>
</div>
<?php  print $feed_icons;   ?>
<a href="http://www.facebook.com/TemplateToaster"class="ttr_footer_facebook" target="_self" >
</a>
<a href="http://www.templatetoaster.com"class="ttr_footer_linkedin" target="_self" >
</a>
<a href="http://twitter.com/templatetoaster"class="ttr_footer_twitter" target="_self" >
</a>
<a href="http://www.templatetoaster.com"class="ttr_footer_googleplus" target="_self" >
</a>
</div>
</div>
</footer>
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
<div class="footer-widget-area">
<?php
if( !empty($page['footerbelowcolumn1'])|| !empty($page['footerbelowcolumn2'])|| !empty($page['footerbelowcolumn3'])||!empty($page['footerbelowcolumn4'])):
?>
<div class="ttr_footer-widget-area_inner_below0">
<?php
$showcolumn= !empty($page['footerbelowcolumn1']);
?>
<?php if($showcolumn): ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerbelowcolumn1">
<?php print render($page['footerbelowcolumn1']); ?>
</div>
</div>
<?php else: ?>
<div class="cell1 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['footerbelowcolumn2']);
?>
<?php if($showcolumn): ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerbelowcolumn2">
<?php print render($page['footerbelowcolumn2']); ?>
</div>
</div>
<?php else: ?>
<div class="cell2 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-sm-block visible-md-block visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['footerbelowcolumn3']);
?>
<?php if($showcolumn): ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerbelowcolumn3">
<?php print render($page['footerbelowcolumn3']); ?>
</div>
</div>
<?php else: ?>
<div class="cell3 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-xs-block">
</div>
<?php
$showcolumn= !empty($page['footerbelowcolumn4']);
?>
<?php if($showcolumn): ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12">
<div class="footerbelowcolumn4">
<?php print render($page['footerbelowcolumn4']); ?>
</div>
</div>
<?php else: ?>
<div class="cell4 col-lg-3 col-md-6 col-sm-6  col-xs-12"  style="background-color:transparent;">
&nbsp;
</div>
<?php endif; ?>
<div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
</div>
</div>
<div class="clearfix"></div>
<?php endif; ?>
</div>
</div>
