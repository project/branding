                        
Installing New Themes-
--------------------- 

To install a new Theme to your Drupal installation, follow these basic steps:-
.............................................................................. 


1. Take the entire Drupal theme folder and place it in your
 Drupal installation under one of the following locations:
 sites/all/themes
 and making it available to the default Drupal site and to all Drupal sites
. You can upload the drupal theme via FTP of TemplateToaster.

2. Log in as an administrator on your Drupal site .

3. Go to the Appearance page
 at admin/appearance. You will see your installed drupal theme listed under the Disabled
 Themes .

4. You can
 optionally make new installed theme as the default theme.

 
Configure Modules:-
------------------

1. Login as administrator.
2. Go to the Structure option located in Admin tool bar at the top of the page.
3. Select �Blocks� option from the menu.
4. Change the settings according to the requirement.
5. Go to the bottom of the page and click �Save Blocks� to save the configuration.
6. Go to home page to see the changes take effect.